/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dbutils;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
   private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());
   public  static  boolean debugFlag = true;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            DBConnector connector = DBConnector.getConnector();
            
            PropertyLoader pLoader = PropertyLoader.getLoader();
            
            Properties pp = pLoader.getPropValues( "dbConnector.properties" );
            
            connector.connect( pp );
            
            //========================================================================
            // Working with user table
            //========================================================================
            UserHandler theUserHandler = UserHandler.getLoader();
            User theUser = theUserHandler.loadFromDB(connector, "selvyn", "gradprog2016");
            
            if( theUser != null )
            {
                System.out.println( theUser.getUserID()+ "//" + theUser.getUserPwd());
            }

            // Convert object to JSON string and save into a file directly
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File("user.json"), theUser);

        } 
        catch (IOException ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  static  void    log( String msg )
    {
        LOGGER.info( msg );
        System.out.println( msg );
    }
    
}
